# Password Change Challenge
Diese SQL-Befehle ermöglichen es eine Challenge einzufügen, die überprüft ob ein User vor einem gewissen Datum sein Passwort geändert hat.

```

SET @challengepoints=300,
    @maxchangedate="2017-06-26 00:00:00";


INSERT INTO Challenge (Title, Description, SolutionType, Solution, SolutionExplanation, BasePoints)
VALUES ('Default Passwort geändert?',
        'Die Cyber Security Challenge läuft nun seit über zwei Wochen. Zeit um auch abseits der eigentlichen Challenges einen kurzen Blick auf die Sensibilität im Umgang mit Zugangsdaten zu werfen.\n\nZu Beginn des Events wurde jeder Teilnehmerin und jedem Teilnehmer ein initiales, sehr schwaches Passwort übermittelt. Schwache Passwörter, noch dazu wenn sie als Klartext in einer Mail übertragen wurden, sollten als \"Einmalpasswörter\" betrachtet, und beim ersten Login geändert werden, auch wenn das System nicht explizit dazu auffordert. \n\nHaben Sie ihr Passwort vor dem 25.06.2017 - 23:59 Uhr geändert, erhalten Sie dafür **300 Punkte** als Bonus.  Sollten Sie ihr Passwort noch nicht geändert haben, bekommen Sie die Bonuspunkte nicht mehr, was Sie aber davon nicht abhalten sollte, das Passwort trotzdem zu ändern.',
        '1',
        'xxxxxxxx',
        '',
        @challengepoints);


SET @challengeid=LAST_INSERT_ID();


INSERT INTO ChallengeSet (Title, Description, Challenges)
VALUES ('Extra Challenge: Passwort Sensibilität',
        'Dies ist eine extra Challenge zum sensiblen Umgang mit Passwörtern für die keine Lösung eingereicht werden kann. Für Details bitte die Challenge öffnen.',
        @challengeid);


INSERT INTO ChallengeAction (challengeid, description, LockChallegenge, removebonus, pointreduction)
VALUES (@challengeid,
        "Sie haben ihr Default-Passwort nicht geändert, daher wird diese Challenge für Sie gesperrt.",
        1,
        1,
        100);


SET @actionid=LAST_INSERT_ID();


INSERT INTO ChallengeSolution (challengeid, userid, solutiontime, solved, solutionentered, wrongentries, points)
SELECT @challengeid AS challengeid,
       id,
       CASE
           WHEN passwordchangedate < @maxchangedate THEN passwordchangedate
           ELSE @maxchangedate
       END AS solutiontime,
       CASE
           WHEN passwordchangedate < @maxchangedate THEN '1'
           ELSE 0
       END AS solved,
       '' AS solutionentered,
       0 AS wrongentries,
       CASE
           WHEN passwordchangedate < @maxchangedate THEN @challengepoints
           ELSE 0
       END AS points
FROM USER;


INSERT INTO ChallengeAccess (userid, challengeid, TYPE, accesstime)
SELECT id,
       @challengeid AS challengeid,
       'Description' AS TYPE,
       CASE
           WHEN passwordchangedate < @maxchangedate THEN passwordchangedate
           ELSE @maxchangedate
       END AS accesstime
FROM USER;


INSERT INTO ChallengeActionAccess (challengeactionid, userid, accesstime, nonce)
SELECT @actionid,
       id,
       @maxchangedate,
       FLOOR(10000000000000000 + RAND() * 9999999999999999)
FROM USER
WHERE passwordchangedate >= @maxchangedate
  OR passwordchangedate IS NULL;
```