Cyber Security Challenge Platform Helper Scripts
================================================

# Overview

This project contains helper scripts for the FH Campus Wien Cyber Security Challenge Platform

# Contribution

Development follows the [git branching model](http://nvie.com/posts/a-successful-git-branching-model/) using the standard prefixes of [git flow](https://github.com/nvie/gitflow).

The format of the Changelog is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

# Usage
