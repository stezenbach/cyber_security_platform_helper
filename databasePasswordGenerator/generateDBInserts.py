#!/usr/bin/python2

# Usage: generateDBInserts.py file.csv
#
# Expected format of CSV file: username;password
#
# john;123456
# jane;secret
#
# The users will automatically be added to the event with ID 1
#

import hashlib,base64,uuid,sys,csv

with open(sys.argv[1], 'rb') as f:
	reader = csv.reader(f,delimiter=';')
	for row in reader:
		m = hashlib.sha256();
		salt = str(uuid.uuid4())[0:12];
		m.update(salt);
		m.update(row[1]);
		print "INSERT INTO User (UserName, Salt, Password, ResetCodeNotAfter) VALUES ('"+row[0]+"', '"+salt+"', '"+base64.b64encode(m.digest())+"', NOW());";
		print "INSERT INTO EventUsers (EventId,UserId) VALUES(1,LAST_INSERT_ID());"
