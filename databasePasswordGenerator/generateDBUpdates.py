#!/usr/bin/python2

# Usage: generateDBUpdates.py file.csv
#
# Expected format of CSV file: username;password
#
# john;123456
# jane;secret
#
# The users will automatically be added to the event with ID 1
#

import hashlib,base64,uuid,sys,csv

with open(sys.argv[1], 'rb') as f:
	reader = csv.reader(f,delimiter=';')
	for row in reader:
		m = hashlib.sha256();
		salt = str(uuid.uuid4())[0:12];
		m.update(salt);
		m.update(row[1]);
		print "UPDATE User SET Salt = '"+salt+"', Password = '"+base64.b64encode(m.digest())+"',  ResetCodeNotAfter = NOW() WHERE User.Username = '"+row[0]+"';";
