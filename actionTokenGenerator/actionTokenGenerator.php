<?php
sendAction(1,19,3,"sparkassencsc");

function sendAction($userId, $challengeId, $actionId, $key) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => "https://its.fh-campuswien.ac.at/cscbackend/users/$userId/challenges/$challengeId/actions/$actionId?format=json",
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => generateActionToken($userId, $challengeId, $actionId, $key)
    ));
    $resp = curl_exec($curl);
    $value = json_decode($resp);
    if(curl_getinfo($curl,CURLINFO_RESPONSE_CODE) == 409 && $value->ResponseStatus->Message != "This action has already been triggered for this user." ) {
        sendAction($userId, $challengeId, $actionId, $key);
    }
    curl_close($curl);
}

function generateActionToken($userId, $challengeId, $actionId, $key) {
    $action = array(
        "UserId" => $userId,
        "ChallengeId" => $challengeId,
        "ActionId" => $actionId,
        "Nonce" => mt_rand(10000000000000000,99999999999999999)
    );
    return array("Nonce" => $action['Nonce'], "Mac" => base64_encode(hash_hmac('sha256',json_encode($action),$key,1)));
}

?>