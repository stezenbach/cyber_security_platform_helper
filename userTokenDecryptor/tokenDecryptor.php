<?php
echo decryptUserToken(10, '{{{UserIdToken-sparkassencsc}}}', 'V7bt9C1AjfjK7Xm7MED4xHWweyywyCR08MbnQZRwoyCWUqUhkgWV3Yp74xeNyR%2FR');
/*
 * This function decrypts a user token that is based on the following format
 * BASE64(AES256-ECB(K, userId) || HMAC-SHA56(K, encryptedId)) where K = HMAC-SHA256({{{UserIdToken-abcdef}}, challengeId)
 */
function decryptUserToken($challengeId, $key, $token) {
    $derivedKey = hash_hmac('sha256', $challengeId, $key, false);
    $rawToken = base64_decode(urldecode($token));
    /*
     * The MAC of the token is validated against HMAC-SHA56(K, encryptedId)
     */
    $validation_tag = hash_hmac('sha256', substr($rawToken, 0, -32), hex2bin($derivedKey), true);
    if($validation_tag != substr($rawToken, -32)) {
        throw new UnexpectedValueException("The MAC does not match.");
    }
    $littleEndian = bin2hex(openssl_decrypt(substr($rawToken, 0, -32), "aes-256-ecb", hex2bin($derivedKey), 1));
    return hexdec(join(array_reverse(str_split($littleEndian,2))));
}

?>
