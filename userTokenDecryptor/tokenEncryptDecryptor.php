<?php
$plainKey = "{{{UserIdToken-sparkassencsc}}}";
$encryption_key = hash_hmac('sha256', '10', $plainKey, false);
echo "Derived Key: ".$encryption_key."\n";
$data = hex2bin("06");
echo bin2hex($data)."\n";
$encrypted = openssl_encrypt($data, "aes-256-ecb", hex2bin($encryption_key), 0);
echo "Encrypted Data: $encrypted\n";

$encrypted_bin = base64_decode($encrypted);
$tag = base64_encode(hash_hmac('sha256', $encrypted_bin, hex2bin($encryption_key), true));
//$tag = openssl_encrypt($encrypted_bin, "aes-256-ecb", hex2bin($encryption_key), 0);
echo "Tag: $tag\n";
$finalTag = base64_encode(base64_decode($encrypted).base64_decode($tag));
$concatinatedTag = base64_decode($encrypted).base64_decode($tag);
//echo "Raw Tag: $concatinatedTag\n";
echo "Complete Tag: ". base64_encode($concatinatedTag)."\n";
$urlEncodedTag = urlencode($finalTag);
echo "URL Tag: " . $urlEncodedTag."\n";


echo "================= STARTING DECODE ===================\n";
$inv_finalTag = urldecode($urlEncodedTag);
echo "Decoded Tag: $inv_finalTag\n";
echo "Validation: ".($inv_finalTag === $finalTag)."\n";

$inv_concatinatedTag = base64_decode($inv_finalTag);
//echo "Raw Tag: $inv_concatinatedTag\n";
echo "Validation: ".($inv_concatinatedTag === $concatinatedTag)."\n";

$inv_tag = base64_encode(substr($inv_concatinatedTag, -32));
echo "Tag: $inv_tag\n";
echo "Validation: ".($inv_tag === $tag)."\n";

$inv_encrypted = base64_encode(substr($inv_concatinatedTag, 0, -32));
echo "Encrypted Tag: $inv_encrypted\n";
echo "Validation: ".($inv_encrypted === $encrypted)."\n";

$inv_data = openssl_decrypt($inv_encrypted, "aes-256-ecb", hex2bin($encryption_key), 0);
echo "Data: ".hexdec(bin2hex($inv_data));
?>
